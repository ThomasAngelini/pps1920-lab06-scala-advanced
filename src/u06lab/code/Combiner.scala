package u06lab.code

/**
  * 1) Implement trait Functions with an object FunctionsImpl such that the code
  * in TryFunctions works correctly.
  *
  * 2) To apply DRY principle at the best,
  * note the three methods in Functions do something similar.
  * Use the following approach:
  * - find three implementations of Combiner that tell (for sum,concat and max) how
  *   to combine two elements, and what to return when the input list is empty
  * - implement in FunctionsImpl a single method combiner that, other than
  *   the collection of A, takes a Combiner as input
  * - implement the three methods by simply calling combiner
  *
  * When all works, note we completely avoided duplications..
 */

trait Functions {
  def sum(a: List[Double]): Double
  def concat(a: Seq[String]): String
  def max(a: List[Int]): Int // gives Int.MinValue if a is empty
}

trait Combiner[A] {
  def unit: A
  def combine(a: A, b: A): A
}

object Combiner{

  def create[A](defaultVal: A, operation: (A,A)=>A): Combiner[A] = new Combiner[A]{
    override def unit: A = defaultVal

    override def combine(a: A, b: A): A = operation(a,b)
  }

  implicit val sumDoubleCombiner: Combiner[Double] = create(0.0,(a:Double, b:Double) => a + b)

  implicit val ConcatStringCombiner: Combiner[String] = create("",(a:String, b:String) => a + b)

  implicit val maxIntCombiner: Combiner[Int] = create(Int.MinValue,(a:Int, b:Int) => Math.max(a,b))

}

object FunctionsImpl extends Functions {

  override def sum(a: List[Double]): Double = a.sum

  override def concat(a: Seq[String]): String = a.foldLeft("")(_ + _)

  override def max(a: List[Int]): Int = a.fold(Int.MinValue)(_ max _)

}

object FunctionsCombinerImpl extends Functions {


  override def sum(a: List[Double]): Double = combiner(a)

  override def concat(a: Seq[String]): String = combiner(a)

  override def max(a: List[Int]): Int = combiner(a)

  private def combiner[A:Combiner](a: Seq[A]):A =
    if (a.isEmpty) implicitly[Combiner[A]].unit
    else implicitly[Combiner[A]].combine(a.head, combiner(a.tail))

}

object TryFunctions extends App {
  val f: Functions = FunctionsImpl

}