package u06lab.code

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._
import u06lab.code.StringUtils._


class ParserTest {

  def parserNTC = new NotTwoConsecutiveParser(Set('X','Y','Z'))
  def parserNTCNE = new BasicParser(Set('X','Y','Z')) with NotTwoConsecutive[Char] with NonEmpty[Char]
  def sparser : Parser[Char] = "abc".charParser()


  @Test
  def testNotTwoConsecutive: Unit ={
    assertEquals(true,parserNTC.parseAll("XYZ".toList))
    assertEquals(false,parserNTC.parseAll("XYYZ".toList))
    assertEquals(true,parserNTC.parseAll("".toList))
  }

  @Test
  def testNotTwoConsecutiveNotEmpty: Unit ={
    assertEquals(true,parserNTCNE.parseAll("XYZ".toList)) // true
    assertEquals(false,parserNTCNE.parseAll("XYYZ".toList)) // false
    assertEquals(false,parserNTCNE.parseAll("".toList)) // false
  }

  @Test
  def testSequenceParser(): Unit ={
    assertEquals(true,sparser.parseAll("aabc".toList)) // true
    assertEquals(false,sparser.parseAll("aabcdc".toList)) // false
    assertEquals(true,sparser.parseAll("".toList)) // true
  }
}
