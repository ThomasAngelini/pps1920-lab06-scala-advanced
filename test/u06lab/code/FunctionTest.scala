package u06lab.code

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._

class FunctionTest {

  val f: Functions = FunctionsImpl
  val fCombiner: Functions = FunctionsCombinerImpl

  @Test
  def testSum(): Unit ={
    assertEquals(fCombiner.sum(List(10.0,20.0,30.1)), f.sum(List(10.0,20.0,30.1))) // 60.1
    assertEquals(fCombiner.sum(List()), f.sum(List()))                // 0.0
  }

  @Test
  def testConcat(): Unit ={
    assertEquals(fCombiner.concat(Seq("a","b","c")),f.concat(Seq("a","b","c")))  // abc
    assertEquals(fCombiner.concat(Seq()),f.concat(Seq()))              //
  }

  @Test
  def testMax(): Unit ={
    assertEquals(fCombiner.max(List(-10,3,-5,0)), f.max(List(-10,3,-5,0)))      // 3
    assertEquals(fCombiner.max(List()), f.max(List()))                // -2147483648
  }


}
